<?php
declare(strict_types=1);

namespace App\Domain\BinPackage;

use GuzzleHttp\Client;
use Nette\Utils\Json;
use Tracy\Debugger;
use Tracy\ILogger;

class ApiClient
{
    private $apiKey;

    private $username;

    private $url;

    private $serializer;

    private $client;

    public function __construct(
        string $apiKey,
        string $username,
        Serializer $serializer,
        string $url = 'https://eu.api.3dbinpacking.com/packer/pack',
        float $timeout = 10.0
    ) {
        $this->apiKey = $apiKey;
        $this->username = $username;
        $this->serializer = $serializer;
        $this->url = $url;

        $this->client = new Client(['timeout' => $timeout]);
    }

    public function setClient(Client $client)
    {
        $this->client = $client;
    }

    public function call(array $boxes, array $products): array // types missing everywhere
    {
        $input = [
            'username' => $this->username,
            'api_key' => $this->apiKey,
            'bins' => $this->serializer->serializeBoxes($boxes),
            'items' => $this->serializer->serializeProducts($products),
        ];

        $result = [];

        $query = Json::encode($input);
        $url = "{$this->url}?query=$query";
        try {
            $res = $this->client->request('GET', $url);
            if ($res->getStatusCode() != 200) { // strict comparison?
                Debugger::log("Error from 3dbinpacking - status code 200", ILogger::ERROR);
                return []; // error that complies with array typehint
            }
            if ($res->getHeader('content-type')[0] != 'application/json') { // strict comparison?
                Debugger::log("Error from 3dbinpacking - wrong content type {$res->getHeader('content-type')[0]}", ILogger::ERROR);
                return [];
            }

            $resData = Json::decode((string)$res->getBody(),Json::FORCE_ARRAY);

            if (!isset($resData['response']['status']) || $resData['response']['status'] != 1) { // no output validation
                Debugger::log("Error from 3dbinpacking - status {resData['status']}", ILogger::ERROR);
                return [];
            }
            if (!isset($resData['response']['bins_packed'])) {
                Debugger::log("Error from 3dbinpacking - missing bins_packed", ILogger::ERROR);
                return [];
            }

            foreach ($resData['response']['bins_packed'] as $packed) {
                if (count($packed['not_packed_items']) == 0) { // strict comparison?
                    $result[] = $packed;
                }
            }

            // dont like this but we we will not do too much with different errors here
        } catch (\Exception $e) { // catch all?
            Debugger::log("Eror in 3dbinpacking - guzzle http issue - {$e->getMessage()}", ILogger::ERROR);
            return [];
        }

        return $result; // api internals exposed as mixed[]
    }
}
