<?php
declare(strict_types=1);

namespace App\Domain\BinPackage;

use App\Domain\Model\Box;
use App\Domain\Model\Product;

class Serializer
{
    public function serializeBox(Box $box): array
    {
        return [
            'w' => $box->getWidth(),
            'h' => $box->getHeight(),
            'd' => $box->getLength(),
            'max_wg' => $box->getWeight(),
            'id' => $box->getId(),
        ];
    }

    public function serializeBoxes(array $boxes)
    {
        return array_map(function(Box $box) {
            return $this->serializeBox($box);
        }, $boxes);
    }

    public function serializeProduct(Product $product): array
    {
        return [
            'w' => $product->getWidth(),
            'h' => $product->getHeight(),
            'd' => $product->getLength(),
            'id' => $product->getId(),
            'vr' => $product->getVerticalRotation() ? 1 : 0,
            'wg' => $product->getWeight(),
            'q' => $product->getQuantity(),
        ];
    }

    public function serializeProducts(array $products): array
    {
        return array_map(function(Product $product) {
            return $this->serializeProduct($product);
        }, $products);
    }
}
