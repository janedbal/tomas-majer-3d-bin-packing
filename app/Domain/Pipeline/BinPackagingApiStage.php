<?php
declare(strict_types=1);

namespace App\Domain\Pipeline;

use App\Domain\BinPackage\ApiClient;
use App\Domain\Model\Box;
use League\Pipeline\StageInterface;

class BinPackagingApiStage implements StageInterface
{
    private $apiClient;

    public function __construct(ApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
    }

    /**
     * @param Payload $payload
     * @return Payload
     */
    public function __invoke($payload)
    {
        if ($payload->outputBox()) { // hack copied multiple times to bypass the pipelines
            return $payload;
        }

        $result = $this->apiClient->call($payload->boxes(), $payload->products());

        // not great - we are using here data from api response...
        if (count($result) > 0) {
            $boxId = $this->smallestBoxId($result);

            $filtered = array_values(array_filter($payload->boxes(), function (Box $box) use ($boxId) {
                return $box->getId() != $boxId;
            }));

            return new Payload($payload->boxes(), $payload->products(), $filtered[0]);
        }

        return $payload;
    }

    private function smallestBoxId(array $boxesApiData): string
    {
        $max = PHP_INT_MAX;
        $boxId = null;
        foreach ($boxesApiData as $boxData) {
            $d = $boxData['bin_data'];
            $size = $d['w'] * $d['h'] * $d['d'];
            if ($size < $max) {
                $max = $size;
                $boxId = $d['id'];
            }
        }
        return $boxId;
    }
}
