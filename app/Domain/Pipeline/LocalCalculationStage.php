<?php
declare(strict_types=1);

namespace App\Domain\Pipeline;

use League\Pipeline\StageInterface;

class LocalCalculationStage implements StageInterface
{
    /**
     * @param Payload $payload
     * @return Payload
     */
    public function __invoke($payload)
    {
        if ($payload->outputBox()) {
            return $payload;
        }

        // Here you can implement own fallback logic for finding right box
        // more info in /Hello.md file

        return $payload;
    }
}
