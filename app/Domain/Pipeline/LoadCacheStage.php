<?php
declare(strict_types=1);

namespace App\Domain\Pipeline;

use App\Domain\Cache\CacheInterface;
use League\Pipeline\StageInterface;

class LoadCacheStage implements StageInterface
{
    private $cache;

    public function __construct(CacheInterface $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @param Payload $payload
     * @return Payload
     */
    public function __invoke($payload)
    {
        if ($payload->outputBox()) {
            return $payload;
        }

        $cachePayload = $this->cache->load($payload);
        if ($cachePayload !== null) {
            return $cachePayload;
        }

        return $payload;
    }
}
