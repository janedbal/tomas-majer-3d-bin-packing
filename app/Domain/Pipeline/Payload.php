<?php
declare(strict_types=1);

namespace App\Domain\Pipeline;

use App\Domain\Cache\IdentifierInterface;
use App\Domain\Model\Box;
use App\Domain\Model\Product;

class Payload
{
    private $boxes;

    private $products;

    private $outputBox;

    public function __construct(array $boxes, array $products, Box $outputBox = null)
    {
        $this->boxes = $boxes;
        $this->products = $products;
        $this->outputBox = $outputBox;

        // wrong place to do this - needed for cache
        usort($this->boxes, function (Box $a, Box $b) {
            return strcmp($a->getId(), $b->getId());
        });
        usort($this->products, function (Product $a, Product $b) {
            return strcmp($a->getId(), $b->getId());
        });
    }

    public function boxes(): array
    {
        return $this->boxes;
    }

    public function products(): array
    {
        return $this->products;
    }

    public function outputBox(): ?Box // hack to bypass the pipelines
    {
        return $this->outputBox;
    }
}
