<?php
declare(strict_types=1);

namespace App\Domain\Model;

use App\Domain\Cache\IdentifierInterface;

class Box implements IdentifierInterface
{
    private string $id;

    private int $width;

    private int $height;

    private int $length;

    private int $weight;

    public function __construct(string $id, int $width, int $height, int $length, int $weight)
    {
        $this->id = $id;
        $this->width = $width;
        $this->height = $height;
        $this->length = $length;
        $this->weight = $weight;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getWidth(): int
    {
        return $this->width;
    }

    public function getHeight(): int
    {
        return $this->height;
    }

    public function getLength(): int
    {
        return $this->length;
    }

    public function getWeight(): int
    {
        return $this->weight;
    }

    public function getIdentifier(): string // needed for cache only, should be implemented in the cache layer
    {
        return "{$this->getId()}|{$this->getWidth()}|{$this->getHeight()}|{$this->getLength()}|{$this->getWeight()}"; // rotation not implemented
    }
}
