<?php
declare(strict_types=1);

namespace App\Domain\Model;

use App\Domain\Cache\IdentifierInterface;

class Product implements IdentifierInterface
{
    private $id;

    private $quantity;

    private $width;

    private $height;

    private $length;

    private $weight;

    private $verticalRotation;

    public function __construct(string $id, int $quantity, int $width, int $height, int $length, int $weight, bool $verticalRotation = true)
    {
        $this->id = $id;
        $this->quantity = $quantity;
        $this->width = $width;
        $this->height = $height;
        $this->length = $length;
        $this->weight = $weight;
        $this->verticalRotation = $verticalRotation;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getWidth(): int
    {
        return $this->width;
    }

    public function getHeight(): int
    {
        return $this->height;
    }

    public function getLength(): int
    {
        return $this->length;
    }

    public function getWeight(): int
    {
        return $this->weight;
    }

    public function getVerticalRotation(): bool
    {
        return $this->verticalRotation;
    }

    public function getIdentifier(): string
    {
        return "{$this->getQuantity()}|{$this->getWidth()}|{$this->getHeight()}|{$this->getLength()}|{$this->getWeight()}|{$this->getVerticalRotation()}";
    }
}