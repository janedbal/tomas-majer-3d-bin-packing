<?php
declare(strict_types=1);

namespace App\Domain\Repository;

use App\Domain\Model\Box;
use Nette\DI\InvalidConfigurationException;

class BoxRepository
{
    private $boxes;

    public function __construct(array $sizes)
    {
        if (count($sizes) == 0) {
            throw new InvalidConfigurationException("No box configured");
        }

        foreach ($sizes as $box) {
            $this->boxes[] = new Box($box[0], $box[1], $box[2], $box[3], $box[4]);
        }
    }

    /**
     * @return Box[]
     */
    public function boxes(): array
    {
        return $this->boxes;
    }
}
