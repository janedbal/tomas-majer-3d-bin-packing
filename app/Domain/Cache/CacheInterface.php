<?php
declare(strict_types=1);

namespace App\Domain\Cache;

use App\Domain\Pipeline\Payload;

interface CacheInterface
{
    public function save(Payload $payload): bool;

    public function load(Payload $payload): ?Payload;

    public function exists(Payload $payload): bool;
}