<?php
declare(strict_types=1);

namespace App\Domain\Cache;

interface IdentifierInterface
{
    public function getIdentifier(): string;
}