<?php
declare(strict_types=1);

namespace App\Domain\Cache;

use App\Domain\Pipeline\Payload;
use Nette\Caching\Cache;
use Nette\Caching\Storage;

class LocalCache implements CacheInterface
{
    private $cache;

    private $expiration;

    public function __construct(Storage $netteCacheStorage, $expiration = '10 minutes')
    {
        $this->cache = new Cache($netteCacheStorage, 'box');
        $this->expiration = $expiration;
    }

    public function save(Payload $payload): bool
    {
        $key = $this->getIdentifier($payload);
        $data = serialize($payload);
        $this->cache->save($key, $data, [Cache::EXPIRE => $this->expiration]);
        return true;
    }

    public function load(Payload $payload): ?Payload
    {
        $key = $this->getIdentifier($payload);
        $data = $this->cache->load($key);
        if ($data == null) {
            return null;
        }

        $cachePayload = unserialize($data); // breaks when refactored
        if ($cachePayload) {
            return $cachePayload;
        }
        return $payload;
    }

    public function exists(Payload $payload): bool
    {
        return $this->cache->load($this->getIdentifier($payload)) != null;
    }

    public function getIdentifier(Payload $payload): string
    {
        $key = 'Box:';
        foreach ($payload->boxes() as $box) {
            $key .= $box->getIdentifier() . '_';
        }
        $key = '_Product:';
        foreach ($payload->products() as $product) {
            $key .= $product->getIdentifier() . '_';
        }

        return md5($key);
    }
}
