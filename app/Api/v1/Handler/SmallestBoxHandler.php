<?php
declare(strict_types=1);

namespace App\Api\v1\Handler;

use App\Domain\BinPackage\ApiClient;
use App\Domain\Model\Product;
use App\Domain\Pipeline\Payload;
use App\Domain\Repository\BoxRepository;
use League\Pipeline\PipelineBuilder;
use Tomaj\NetteApi\Handlers\BaseHandler;
use Tomaj\NetteApi\Params\JsonInputParam;
use Tomaj\NetteApi\Response\JsonApiResponse;
use Tomaj\NetteApi\Response\ResponseInterface;

class SmallestBoxHandler extends Basehandler
{
    private $boxRepository;

    private $pipelineBuilder;

    private $apiClient;

    public function __construct(BoxRepository $boxRepository, PipelineBuilder $pipelineBuilder, ApiClient $apiClient)
    {
        parent::__construct();
        $this->boxRepository = $boxRepository;
        $this->pipelineBuilder = $pipelineBuilder;
        $this->apiClient = $apiClient;
    }

    public function params(): array
    {
        return [
            (new JsonInputParam('raw', file_get_contents(__DIR__ . '/InputSchema.json')))->setRequired(),
        ];
    }

    private function prepareProducts(array $inputProducts)
    {
        $result = [];
        foreach ($inputProducts as $productData) {
            $key = "{$productData['width']}|{$productData['height']}|{$productData['length']}|{$productData['weight']}";

            if (isset($result[$key])) {
                $op = $result[$key];
                $result[$key] = Product::fromSelf($op);
            } else {
                $result[$key] = new Product($key, 1, $productData['width'], $productData['height'], $productData['length'], $productData['weight']);
            }
        }
        return array_values($result);
    }

    public function handle(array $params): ResponseInterface
    {
        $products = $this->prepareProducts($params['raw']['input']);

        $boxes = $this->boxRepository->boxes();

        $payload = new Payload($boxes, $products);
        $pipeline = $this->pipelineBuilder->build();
        $result = $pipeline->process($payload);

        $box = $result->outputBox();

        if ($box === null) {
            return new JsonApiResponse(404, ['status' => 'error', 'code' => 'box_no_available']);
        }

        return new JsonApiResponse(200, ['status' => 'ok', 'box' => [
            'width' => $box->getWidth(),
            'height' => $box->getHeight(),
            'length' => $box->getLength(),
        ]]);
    }
}
