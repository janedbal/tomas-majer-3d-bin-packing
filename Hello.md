General info
------------

Microservice je bez stavu, ak pride iny input tak bude iny output, cache sa neaplikuje.
neukladam data o produktoch a krabiciach, je to mozne menit _on the fly_ - cache sa po case zmaze

Implement in Nette framework.

Concepts
--------

+ [Nette api](https://github.com/tomaj/nette-ap)
    1. decouple api concepts into Handler + Authorizator + Identifier
    2. adding mupltiple other things like validation on input and output, console etc...
+ [Pipeline](https://github.com/thephpleague/pipeline)
    1. Probably there will be multiple pipelines in microservice? some naming change will be great
    2. it is a little bit "bended" more in refactoring part
+ Cache
    1. Implemented wrapper on nette cache with filesystem backend
    2. Can be replaced with _anything_
+ Box storage
    1. right now in config, for changing boxes you have re-deploy app
    2. can be easily replaced with DB (but it will be other dependency...)

Testing
----------
- everything should be ok
- remote call with guzzle - client can be injected from test and force remote outputs
- *one thing to change* ApiClient is responding with array from remote call and this array is processed in *BinPackagingApiStage*, for not if we are solving only this particular task it is ok, but in the future it will be great to decouple it 

Refactoring:
------------
- dont like validation and processing input in ApiClient. i would like to introduce some general validation with schema to make sure that input is valid (it is in nette 3.0). also put there better exception handling with reporting to external storage.
- remove arrays for *boxes* and *products* and use some collection framework - code will be simplier and things like sorting, finding element will be easier
- I would like to introduce better payload class and put there some option to register some custom info/tags from stages. it will not be that nice like it is now but there will be more possibility, and it will be quicker (checking if the response was loaded from cache for example)
- also quantity in product domain model is not so nice
- using int everywhere, maybe it isn't so good - need more domain knowledge
- introduce some kind of internal tracking + counting to get insight about how cache is doing etc... probably datadog
- add output api validator with json schema as it is for input
- make it more robust about api and not going for the smallest box but put this logic on own place
- there is one serializer for 2 objects - broken single responsibility principle ;-)

Validations
-----------

### Input output
- add more validation is it is mentioned on refactoring above

### How to solve issue with broken dbinpacking.com?

In last stage of pipeline there is possibility to implement own fallback logic for finding right box.

There are 2 cases that we need to cover here:

1. there is outage/missconfig/something with remote service
    - implement own simplier logic in last stage (_LocalCalculationStage_)
2. remote service wasn't able to find box
    1. get bigger *dummy* box ;-) and return always on some special case
    2. multi-bin packaging - split products into boxes - there is api for it
    3. introduce state with "cannot deliver *some* product"
         - maybe bigger one, or cheaper one. we will need to change api for this
         - and it will be MUCH more complicated here and also on client :-(

